# React Native setup

### Getting Started

##### Installing dependencies
- iOS
    You will need Node, Watchman, the React Native command line interface, and Xcode.
- Android
    You will need Node, Watchman, the React Native command line interface, a JDK, and Android Studio.

This [page](https://facebook.github.io/react-native/docs/getting-started)  will help you install and build your first React Native app.

### Installing

``` 
yarn install
```

### Prerequisites

##### Android
 
Go to your react-native Project then go to android directory Create a file with this name local.properties
Open the file and paste your Android SDK path like below :
- For windows users:
```
sdk.dir=C\:\\Users\\UserName\\AppData\\Local\\Android\\sdk
```
Replace UserName with your pc user name . Also make sure the folder is sdk or Sdk. In my case my computer user name is Zahid so the path look like :
```
sdk.dir=C\:\\Users\\Zahid\\AppData\\Local\\Android\\sdk
```
 - For Mac users:
```
sdk.dir = /Users/USERNAME/Library/Android/sdk
```
Where USERNAME is your OSX username
 - For Linux (Ubuntu) users:
```
sdk.dir = /home/USERNAME/Android/Sdk
```
Where USERNAME is your linux username(Linux paths are case-sensitive: make sure the case of S in Sdk matches)

Also check if you have install adb command, if not [here](https://stackoverflow.com/questions/31374085/installing-adb-on-macos) you have more information.

## Create first build
- iOS
```
yarn ios:install
yarn ios:run
```

- Android
```
yarn android:install
yarn android:run
```

## Running the tests
```
yarn test
```


## Deployment
 - iOS
```
yarn ios:run-release // --device "device name"
```
 - Android
```
yarn android:run-release
yarn build:release
cd /addroid/app/build/outputs/apk/release
adb devices // copy deviceIS
adb -s <deviceId> install <apk>
```


#### Another helpful commands
!!! This command can save lives if it is used constantly
```
update:react-native 
```
```
yarn eslint
```
```
yarn ios:run --device '<iPhoneName>' // install the build on 
```

```
yarn android:link // depricated from 0.59
```


## Author

Diana Turnea (turnea.diana@gmail.com)
