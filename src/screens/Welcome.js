import React, { Component } from 'react'
import { Image } from 'react-native'
import { Container, Section } from '../components/StyledViews.js'
import { Title, Text } from '../components/StyledText.js'
import { images } from './../common'
export default class Welcome extends Component {
  render() {
    return (
      <Container
        justifyContent="space-between"
        paddingTop={48}
        paddingBottom={24}
      >
        <Section>
          <Image source={images.logo} resizeMode="cover" />
        </Section>
        <Section justifyContent="flex-start">
          <Title> Thinslices </Title>
          <Text>
            Lorem Ipsum is simply dummy text of the printing and typesetting.
            Lorem Ipsum has been the industry's standard dummy text ever since
            the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book.
          </Text>
        </Section>
      </Container>
    )
  }
}
