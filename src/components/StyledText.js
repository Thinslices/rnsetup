import styled from 'styled-components'
import { colors } from '../common'

export const Title = styled.Text`
  color: ${colors.text};
  font-size: 48;
  padding: 12px;
`

export const Text = styled.Text`
  color: ${colors.text};
  font-size: 18;
  padding: 24px;
`
