import styled from 'styled-components'
import { colors } from '../common'

import { Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

export const Container = styled.View`
  background-color: ${colors.background};
  flex: 1;
  flex-direction: column;
  width: ${width};
  ${props => props.height && `height: ${props.height}`};
  align-items: ${props => (props.alignItems ? props.alignItems : 'center')};
  justify-content: ${props =>
    props.justifyContent ? props.justifyContent : 'center'};
  ${props => props.paddingTop && `padding-top: ${props.paddingTop}`};
  ${props => props.paddingBottom && `padding-bottom: ${props.paddingBottom}`};
  ${props => props.marginTop && `margin-top: ${props.marginTop}`};
  ${props => props.marginBottom && `margin-bottom: ${props.marginBottom}`};
  ${props => props.marginRight && `margin-right: ${props.marginRight}`};
  ${props => props.marginLeft && `margin-left: ${props.marginLeft}`};
`
export const Section = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: ${props =>
    props.justifyContent ? props.justifyContent : 'center'};
  align-items: center;
`
